/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sadb2.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author lashanc
 */
public class SystemConfigData {

    private static String activeUser;
    private static String userType;
    private static String empID;

    public static String getActiveUser() {
        return activeUser;
    }

    public static void setActiveUser(String aActiveUser) {
        activeUser = aActiveUser;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String aUserType) {
        userType = aUserType;
    }

    public static String getEmpID() {
        return empID;
    }

    public static void setEmpID(String aEmpID) {
        empID = aEmpID;
    }

}

class date {

    public static void main(String[] args) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Calendar currentCal = Calendar.getInstance();
        String currentdate = dateFormat.format(currentCal.getTime());
        currentCal.add(Calendar.MONTH, 1);
        String toDate = dateFormat.format(currentCal.getTime());
        System.out.println(currentdate);
        System.out.println(toDate);
    }
}
