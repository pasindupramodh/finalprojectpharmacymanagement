/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.comon;

/**
 *
 * @author ASUS
 */
import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;


public class SystemLogger {
    public static void initLogger(){
        try {
                String path="D:/My_Logger.html";
       HTMLLayout hTMLLayout = new HTMLLayout();
   //    PatternLayout patternLayout = new PatternLayout("%p %d{yyyy-MMM-dd HH:mm:ss, SSS} %C %M %m %n");
        RollingFileAppender appender = new RollingFileAppender(hTMLLayout, path);
        appender.setMaxFileSize("1MB");
        appender.setName("Mylogger");
        appender.activateOptions();
        Logger.getRootLogger().addAppender(appender);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        initLogger();
        Logger log = Logger.getLogger("Mylogger");
        log.info("Logger Initiate");
    }
}