/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.comon;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author ASUS
 */
public class Icons {
    private static ImageIcon Success;
    private static ImageIcon Error;
    private static ImageIcon Warning;
    private static ImageIcon Qusetion;
    public static ImageIcon getSuccess() {
        setSuccess();
        return Success;
    }

    public static void setSuccess() {
        Icons i = new Icons();
        Success = i.Ico("/Icon/success.png");
    }

    public static ImageIcon getError() {
        setError();
        return Error;
    }

    public static void setError() {
        Icons i = new Icons();
        Error =i.Ico("/Icon/error.png");
    }

    public static ImageIcon getWarning() {
        setWarning();
        return Warning;
    }

    public static void setWarning() {
        Icons i = new Icons();
        Warning = i.Ico("/Icon/icons8_warning_50px_1.png");
    }

    public static ImageIcon getQusetion() {
        setQusetion();
        return Qusetion;
    }

    public static void setQusetion() {
        Icons i = new Icons();
        Qusetion = i.Ico("/Icon/question-mark.png");
    }
    public  ImageIcon Ico(String path){
       Image img = null;
        try {
            InputStream in = getClass().getResourceAsStream(path);
            img = ImageIO.read(in);
            img = img.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            Logger.getLogger(Icons.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ImageIcon(img);
    }
    
}
