/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.comon;


import com.FinalProjectPharmacy.db.DB;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
 
/**
 *
 * @author ASUS
 */
public class AddDataFromDB {
    
    public static void addDataToCombo(JComboBox combo ,String tableName,String whereQuery, String... s){
        try {
            ResultSet rs = DB.searchDataFromDB("select * from "+tableName+" "+whereQuery);
            Vector v = new Vector();
            String name = "";
            while(rs.next()){
                
                for (String string : s) {
                   if(!string.contains(",")){
                       name +=(name.trim()+rs.getString(string)+"-" );
                       
//                       v.add(name);
                   }else{
                       
                       String []na = string.split(",");
                       for (String string1 : na) {
                           name+=rs.getString(string1)+" ";
                       }
                       
                   }
                   
                }
                if(name.startsWith("-")){
                    name = name.substring(1, name.length());
                }
                if(name.endsWith("-")){
                    name = name.substring(0, name.length()-1);
                }
                if(name.contains("-")){
                    name=name.split("-")[1]+" - "+name.split("-")[2];
                }
                v.add(name);
                   name="";
                
//                for(int i = 0 ;i<s.length;i++){
//                    if(s[i].contains(",")){
//                        String[] split = s[i].split("'");
//                        for (String string : split) {
//                            asss+=(rs.getString(string)+" ");
//                        }
//                        
//                    }else{
//                        asss+=(rs.getString(s[i])+"-");
//                    }
//                    
//                   
//                }
//                 v.add(asss);
//                 asss="";
            }
            combo.setModel(new DefaultComboBoxModel(v));
        } catch (Exception ex) {
            Logger.getLogger(AddDataFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void addDataToTable(JTable table,String tableName,String joinQuery,String whereSQLQuery,String... s){
        
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        for (int i = 0;i<dtm.getRowCount();){
            dtm.removeRow(i);
        }
        try {
            ResultSet rs = DB.searchDataFromDB("select * from "+tableName+" "+joinQuery+" "+whereSQLQuery);
            while(rs.next()){
                Vector v = new Vector();
                for (String string : s) {
                   if(string.contains(",")){
                       String []na = string.split(",");
                       String name="";
                       for (String string1 : na) {
                           name+=rs.getString(string1)+" ";
                       }
                       v.add(name);
                   }else{
                       v.add(rs.getString(string));
                   }
                }
                dtm.addRow(v);
            }
        } catch (Exception ex) {
            Logger.getLogger(AddDataFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void addDataToList(JList list, String tableName, String columnName ,String Value ){
        try {
            ResultSet rs = DB.searchDataFromDB("select "+columnName+" from "+tableName+" where "+columnName+" like '"+Value+"%' and isactive='1'");
            Vector v = new Vector();
            while(rs.next()){
                v.add(rs.getString(columnName));
            }
            list.setListData(v);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        JList list = new JList();
        
    }
    public static void addSearchDataToTable(JTable table,String tableName,String joinQuery,String likeSQLQuery,String... s){
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        for (int i = 0;i<dtm.getRowCount();){
            dtm.removeRow(i);
        }
        try {
            ResultSet rs = DB.searchDataFromDB("select * from "+tableName+" "+joinQuery+" "+likeSQLQuery);
            while(rs.next()){
                Vector v = new Vector();
                for (String string : s) {
                   if(string.contains(",")){
                       String []na = string.split(",");
                       String name="";
                       for (String string1 : na) {
                           name+=rs.getString(string1)+" ";
                       }
                       v.add(name);
                   }else{
                       v.add(rs.getString(string));
                   }
                }
                dtm.addRow(v);
            }
        } catch (Exception ex) {
            Logger.getLogger(AddDataFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void addSearchDataToCombo(JComboBox combo ,String tableName,String likeQuery,String... s){
        try {
            ResultSet rs = DB.searchDataFromDB("select * from "+tableName+" "+likeQuery);
            Vector v = new Vector();
            String asss="";
            while(rs.next()){
                for(int i = 0 ;i<s.length;i++){
                    asss+=(rs.getString(s[i])+" ");
                   
                }
                 v.add(asss);
                 asss="";
            }
            combo.setModel(new DefaultComboBoxModel(v));
        } catch (Exception ex) {
            Logger.getLogger(AddDataFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
