/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.charts;

/**
 *
 * @author ASUS
 */
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class LineChart extends ApplicationFrame {

    String chartTitle;
    DefaultCategoryDataset dataSet;

    public LineChart(String title) {
        super(title);
    }

    public static void addLineChart(String chartTitle, CategoryDataset dataset1, Container container, Color color) {

//        chartTitle = "Monthly Income";
        String categoryAxisLabel = "Month";
        String valueAxisLabel = "Price (Rs)";
        CategoryDataset dataset = dataset1;

        JFreeChart chart = ChartFactory.createLineChart3D(chartTitle, categoryAxisLabel, valueAxisLabel, dataset, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot plot = chart.getCategoryPlot();
        LineAndShapeRenderer renderer = new LineAndShapeRenderer();
        plot.setRenderer(renderer);
        plot.setOutlinePaint(Color.gray);
        plot.setOutlineStroke(new BasicStroke(2.0f));
        plot.setBackgroundPaint(new Color(51, 51, 51));
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.lightGray);

        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.lightGray);
        // sets paint color for each series
        renderer.setSeriesPaint(0, Color.white);
//        renderer.setSeriesPaint(1, Color.GREEN);
//        renderer.setSeriesPaint(2, Color.BLUE);
//        renderer.setSeriesPaint(3, Color.YELLOW);

// sets thickness for series (using strokes)
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));
//        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
//        renderer.setSeriesStroke(2, new BasicStroke(2.0f));
//        renderer.setSeriesStroke(3, new BasicStroke(1.5f));

        container.setLayout(new java.awt.BorderLayout());
        ChartPanel CP = new ChartPanel(chart);
        container.add(CP, BorderLayout.CENTER);
//        JFreeChart chart = ChartFactory.createXYLineChart("Monthly Income", "Months", "Income",
//                xyDataset, PlotOrientation.VERTICAL, true, true, true);
//        XYPlot plot = (XYPlot) chart.getPlot();
//        plot.setForegroundAlpha(0.9f);
//        plot.setRangeGridlinesVisible(true);
//        plot.setRangeGridlinePaint(Color.BLACK);
//
//        plot.setDomainGridlinesVisible(true);
//        plot.setDomainGridlinePaint(Color.BLACK);
//
//        String[] grade = new String[12];
//        grade[0] = "January";
//        grade[1] = "February";
//        grade[2] = "March";
//        grade[3] = "April";
//        grade[4] = "May";
//        grade[5] = "June";
//        grade[6] = "July";
//        grade[7] = "August";
//        grade[8] = "September";
//        grade[9] = "October";
//        grade[10] = "November";
//        grade[11] = "December";
//        SymbolAxis rangeAxis = new SymbolAxis("Months", grade);
//
//        rangeAxis.setTickUnit(new NumberTickUnit(1));
//        rangeAxis.setRange(0, grade.length);
//        plot.setDomainAxis(rangeAxis);
//        XYItemRenderer renderer = chart.getXYPlot().getRenderer();
//        renderer.setSeriesPaint(0, color);
//        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
//        renderer.setBaseItemLabelFont(new Font("Times New Romans", 2, 18));

    }
}
