/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class DB {
    private static Connection con;
    
    private static void con() throws ClassNotFoundException,SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://:3309/pharmacy_final","root","1234");
    }
    public static void pusDataToDB(String sql)throws SQLException,ClassNotFoundException{
        if(con==null){
            con();
        }
        con.createStatement().executeUpdate(sql);
    }
    public static ResultSet searchDataFromDB(String sql) throws SQLException,ClassNotFoundException{
        if(con==null){
            con();
        }
        return con.createStatement().executeQuery(sql);
    }
    public static void main(String args[]){
        
    }

    public static Connection getDBConnection() throws ClassNotFoundException, SQLException {
        if(con==null){
            con();
        }
        return con;
    }
}
