/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FinalProjectPharmacy.db;

import java.awt.Component;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class DBConfigBackup {

    private Component Dbbacup;
    public boolean backupDataWithDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
        boolean status = false;
        try {
            dumpExePath = "C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysqldump.exe";
            Process p = null;
            host = "localhost";
            port = "3309";
            user = "root";
            password = "1234";;
//            database = "mssphasetwodbr1.02";
//            host = DataSourceConfig.getDataSource().getHost();
//            port = DataSourceConfig.getDataSource().getPort();
//            user = DataSourceConfig.getDataSource().getUsername();
//            password = DataSourceConfig.getDataSource().getPassword();
            database = "pharmacy_final";
            
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
//            String filepath = "backup(with_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
            String filepath = backupPath + ".sql";

//            String batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
            String batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + filepath + "\"";

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec(batchCommand);
            int processComplete = p.waitFor();

            if (processComplete == 0) {
                status = true;
                try {
                    JOptionPane.showMessageDialog(Dbbacup, "Bacup Successful","Success",JOptionPane.INFORMATION_MESSAGE);
                }catch(Exception e){
                e.printStackTrace();
                }
                
            } else {
                status = false;
               try {
                    JOptionPane.showMessageDialog(Dbbacup, "Can't Bacup Your Database","error",JOptionPane.ERROR_MESSAGE);
                }catch(Exception e){
                e.printStackTrace();
                }
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return status;
    }
    public static void main(String[] args) {
        DBConfigBackup bConfigBackup = new DBConfigBackup();
        bConfigBackup.backupDataWithDatabase("", "", "", "", "", "", "D:/abc"+new Date().getTime());
    }
    
}